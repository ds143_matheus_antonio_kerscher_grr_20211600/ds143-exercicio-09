#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

enum Color
{
  RED,
  BLACK
};

typedef struct Node
{
  int valor;
  enum Color cor;
  struct Node *esq;
  struct Node *dir;
  struct Node *pai;
} Arvore_Red_Black;

Arvore_Red_Black *criar_no(int valor);
bool is_red(Arvore_Red_Black *no);
Arvore_Red_Black *giro_esquerdo(Arvore_Red_Black *no);
Arvore_Red_Black *giro_direito(Arvore_Red_Black *no);
void troca_cores(Arvore_Red_Black *no);
Arvore_Red_Black *inserir_recursivo(Arvore_Red_Black *raiz, int valor);
Arvore_Red_Black *inserir(Arvore_Red_Black *raiz, int valor);
Arvore_Red_Black *remover_recursivo(Arvore_Red_Black *raiz, int valor);
Arvore_Red_Black *remover(Arvore_Red_Black *raiz, int valor);

Arvore_Red_Black *criar_no(int valor)
{
  Arvore_Red_Black *no = (Arvore_Red_Black *)malloc(sizeof(Arvore_Red_Black));
  no->valor = valor;
  no->cor = RED;
  no->esq = no->dir = no->pai = NULL;
  return no;
}

bool is_red(Arvore_Red_Black *no)
{
  return no && no->cor == RED;
}

Arvore_Red_Black *giro_esquerdo(Arvore_Red_Black *no)
{
  Arvore_Red_Black *no_d = no->dir;
  no->dir = no_d->esq;
  no_d->esq = no;
  no_d->cor = no->cor;
  no->cor = RED;
  return no_d;
}

Arvore_Red_Black *giro_direito(Arvore_Red_Black *no)
{
  Arvore_Red_Black *no_e = no->esq;
  no->esq = no_e->dir;
  no_e->dir = no;
  no_e->cor = no->cor;
  no->cor = RED;
  return no_e;
}

void troca_cores(Arvore_Red_Black *no)
{
  no->cor = !no->cor;
  no->esq->cor = !no->esq->cor;
  no->dir->cor = !no->dir->cor;
}

Arvore_Red_Black *inserir_recursivo(Arvore_Red_Black *raiz, int valor)
{
  if (!raiz)
    return criar_no(valor);

  if (valor < raiz->valor)
    raiz->esq = inserir_recursivo(raiz->esq, valor);
  else
    raiz->dir = inserir_recursivo(raiz->dir, valor);

  if (is_red(raiz->dir) && !is_red(raiz->esq))
    raiz = giro_esquerdo(raiz);
  if (is_red(raiz->esq) && is_red(raiz->esq->esq))
    raiz = giro_direito(raiz);
  if (is_red(raiz->esq) && is_red(raiz->dir))
    troca_cores(raiz);

  return raiz;
}

Arvore_Red_Black *inserir(Arvore_Red_Black *raiz, int valor)
{
  raiz = inserir_recursivo(raiz, valor);
  raiz->cor = BLACK;
  return raiz;
}

Arvore_Red_Black *remover_recursivo(Arvore_Red_Black *raiz, int valor)
{
  if (!raiz)
    return NULL;

  if (valor < raiz->valor)
  {
    if (!is_red(raiz->esq) && !is_red(raiz->esq->esq))
      raiz = giro_esquerdo(raiz);
    raiz->esq = remover_recursivo(raiz->esq, valor);
  }
  else
  {
    if (is_red(raiz->esq))
      raiz = giro_direito(raiz);
    if (valor == raiz->valor && !raiz->dir)
      return NULL;
    if (!is_red(raiz->dir) && !is_red(raiz->dir->esq))
      raiz = giro_direito(raiz);
    if (valor == raiz->valor)
    {
      Arvore_Red_Black *no = menor_no(raiz->dir); // aqui deverá retornar o menor no da árvore (função não implementada)
      raiz->valor = no->valor;
      raiz->dir = remover_recursivo(raiz->dir, no->valor);
    }
    else
      raiz->dir = remover_recursivo(raiz->dir, valor);
  }

  return balancear(raiz); // acabei não implementando essa função por não saber como balancear a árvore
}

Arvore_Red_Black *remover(Arvore_Red_Black *raiz, int valor)
{
  raiz = remover_recursivo(raiz, valor);
  if (raiz)
    raiz->cor = BLACK;
  return raiz;
}
